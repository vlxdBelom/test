import template from './left-menu-element.hbs';
import './left-menu-element.scss';

const LeftMenuElement = ({href, elementName}) => {
  return template({href: href, elementName: elementName});
};

export default LeftMenuElement;
