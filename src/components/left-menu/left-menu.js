import template from './left-menu.hbs';
import './left-menu.scss';
import LeftMenuElement from "./left-menu-element/left-menu-element";

const LeftMenu = () => {
  const elements = ['Напутсвенное слово', 'Сетка', 'Типографика', 'UI'];
  const hrefs = ['#test task', '#grid', '#typography', '#ui'];

  const menuItems = elements.map((elem, index) => {
    return LeftMenuElement({href: hrefs[index], elementName: elem});
  });

  const renderedMenu = menuItems.join('');

  return template({
    children: renderedMenu
  });
};

export default LeftMenu;


