import template from './grid.hbs';
import './grid.scss';
import GridElement from "./grid-element/grid-element";
import GridDescElement from "./grid-element/grid-desc-element/grid-desc-element";
import DesktopImage from "../../../../assets/images/desktop.png";
import TabletImage from "../../../../assets/images/tablet.png";
import PhoneImage from "../../../../assets/images/phone.png";

const Grid = () => {
  const gridContent = [];
  const descNames = ["Контент", "Колонок", "Ширина колонки", "Межколонник"];

  const desktop = {
    name: "Десктоп (1200+)", descValues: ["1072-1312", "12", "60-80", "32"]
  }

  const tablet = {
    name: "Планшет (736+)", descValues: ["672-928", "8", "56-88", "32"]
  }

  const phone = {
    name: "Телефон (320+)", descValues: ["296-424", "4", "56-88", "24"]
  }

  const desktopGridChildren = descNames.map((name, index) => {
    return GridDescElement({name: name, desc: desktop.descValues[index]})
  });

  const tabletGridChildren = descNames.map((name, index) => {
    return GridDescElement({name: name, desc: tablet.descValues[index]})
  });

  const phoneGridChildren = descNames.map((name, index) => {
    return GridDescElement({name: name, desc: phone.descValues[index]})
  });

  const desktopGridElement = GridElement({
    gridElementName: desktop.name, children: desktopGridChildren.join(''), image: DesktopImage
  });
  const tabletGridElement = GridElement({
    gridElementName: tablet.name, children: tabletGridChildren.join(''), image: TabletImage
  });
  const phoneGridElement = GridElement({
    gridElementName: phone.name, children: phoneGridChildren.join(''), image: PhoneImage
  });

  return template({gridContent: [desktopGridElement, tabletGridElement, phoneGridElement].join('')});
}

export default Grid;
