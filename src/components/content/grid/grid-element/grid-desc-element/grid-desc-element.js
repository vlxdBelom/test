import template from './grid-desc-element.hbs';
import './grid-desc-element.scss';

const GridDescElement = ({name, desc}) => {
  return template({name: name, desc: desc});
}

export default GridDescElement;
