import template from './grid-element.hbs';
import './grid-element.scss';

const GridElement = ({gridElementName, children, image}) => {

  return template({gridElementName, children, image});
}

export default GridElement;
