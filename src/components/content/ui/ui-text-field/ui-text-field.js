// ui-text-field.js
import template from './ui-text-field.hbs';
import './ui-text-field.scss';

const UiTextField = ({state, label, value, errorMessage}) => {
  return template({state, label, value, errorMessage});
}

export default UiTextField;
