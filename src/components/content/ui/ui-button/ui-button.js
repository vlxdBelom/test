import template from './ui-button.hbs';
import './ui-button.scss';

const UiButton = (state) => {
  return template(state);
}

export default UiButton;
