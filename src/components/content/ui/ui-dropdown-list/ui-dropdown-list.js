import template from './ui-dropdown-list.hbs';
import './ui-dropdown-list.scss';

const UiDropdownList = ({state, label, options}) => {
  return template({state, label, options});
}

export default UiDropdownList;
