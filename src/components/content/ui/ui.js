import template from './ui.hbs';
import './ui.scss';
import UiTextField from "./ui-text-field/ui-text-field";
import UiDropdownList from "./ui-dropdown-list/ui-dropdown-list";
import UiButton from "./ui-button/ui-button";
import uiCheckbox from "./ui-checkbox/ui-checkbox";
import UiAccordion from "./ui-accordion/ui-accordion";
import uiRadioButton from "./ui-radio-button/ui-radio-button";

const Ui = () => {
  const textFields = [];
  textFields.push(UiTextField({label: "E-mail", value: "", state: "default"}));
  textFields.push(UiTextField({label: "E-mail", value: "", state: "hover"}));
  textFields.push(UiTextField({label: "E-mail", value: "office@make.st", state: "input"}));
  textFields.push(UiTextField({label: "E-mail", value: "officemake.st"}));

  const dropdownLists = [];
  const dropdownData = {
    state: "",
    label: "Выберите что-нибудь",
    options: ["Первый пункт", "Второй пункт", "Выбранный пункт", "Теорема Эскобара",]
  };

  dropdownLists.push(UiDropdownList({...dropdownData}));
  dropdownLists.push(UiDropdownList({...dropdownData}));
  dropdownLists.push(UiDropdownList({...dropdownData}));

  const checkboxesList = [];
  checkboxesList.push(uiCheckbox({id: 1, label: "Выбери меня", checked: false}))
  checkboxesList.push(uiCheckbox({id: 2, label: "Выбери меня", checked: false}))
  checkboxesList.push(uiCheckbox({id: 3, label: "Птица счастья завтрашнего дня", checked: true}))

  const radioList = [];
  radioList.push(uiRadioButton({id: 4, name: 1, label: "Пластмассовый мир победил", checked: false}));
  radioList.push(uiRadioButton({id: 5, name: 1, label: "Макет оказался сильней", checked: false}));
  radioList.push(uiRadioButton({id: 6, name: 1, label: "Последний кораблик остыл", checked: true}));

  const data = {
    items: [
      {
        title: 'Аккордеон', content: 'Гоpдость полными вагонами золотыми погонами\n' +
          'С юга дyют молодые вет…\n' +
          'Pазpывая в клочья облака не забыли шлют из далека\n' +
          'С дома мама И не последняя любовь\n' +
          'А по небy бегyт видишь чьи-то следы\n' +
          'Это может быть ты это может быть я\n' +
          'Это может наш дрyг\n' +
          'Это может нам поют свои'
      },
      {
        title: 'Аккордеон', content: 'Гоpдость полными вагонами золотыми погонами\n' +
          'С юга дyют молодые вет…\n' +
          'Pазpывая в клочья облака не забыли шлют из далека\n' +
          'С дома мама И не последняя любовь\n' +
          'А по небy бегyт видишь чьи-то следы\n' +
          'Это может быть ты это может быть я\n' +
          'Это может наш дрyг\n' +
          'Это может нам поют свои'
      },
      {
        title: 'Аккордеон', content: 'Гоpдость полными вагонами золотыми погонами\n' +
          'С юга дyют молодые вет…\n' +
          'Pазpывая в клочья облака не забыли шлют из далека\n' +
          'С дома мама И не последняя любовь\n' +
          'А по небy бегyт видишь чьи-то следы\n' +
          'Это может быть ты это может быть я\n' +
          'Это может наш дрyг\n' +
          'Это может нам поют свои'
      },
    ]
  };

  const buttonsList = [];
  buttonsList.push(UiButton(), UiButton(), UiButton(), UiButton({state: "disabled"}));

  return template({
    textField: textFields.join(''),
    dropdownList: dropdownLists.join(''),
    button: buttonsList.join(''),
    check: checkboxesList.join(''),
    radio: radioList.join(''),
    accordion: UiAccordion(data)
  });
}

export default Ui;
