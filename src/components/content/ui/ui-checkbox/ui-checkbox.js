import template from './ui-checkbox.hbs';
import './ui-checkbox.scss';

const UiCheckbox = ({id, label, checked}) => {
  return template({id, label, checked});
}

export default UiCheckbox;
