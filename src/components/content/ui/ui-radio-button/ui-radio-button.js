import template from './ui-radio-button.hbs';
import './ui-radio-button.scss';

const UiRadioButton = ({id, name, label, checked}) => {
  return template({id, name, label, checked});
}

export default UiRadioButton;
