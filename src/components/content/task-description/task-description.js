import template from './task-description.hbs';
import './task-description.scss';

const TaskDescription = () => {
  return template();
}

export default TaskDescription;
