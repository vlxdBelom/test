import template from './header-burger-button.hbs';
import './header-burger-button.scss';

const HeaderBurgerButton = () => {
  return template();
};

export default HeaderBurgerButton;
