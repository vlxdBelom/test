// header.js
import template from './header.hbs';
import HeaderPhotoDesc from './header-photo-desc/header-photo-desc';
import HeaderButton from './header-burger-button/header-burger-button';
import './header.scss';
import HeaderMenu from "../header-menu/header-menu";

const Header = () => {
  return template({
    HeaderPhotoDesc: HeaderPhotoDesc(),
    HeaderButton: HeaderButton(),
    HeaderMenu: HeaderMenu()
  });
};

export default Header;
