import template from './header-photo-desc.hbs';
import logo from '../../../../assets/images/logo.png';
import './header-photo-desc.scss';

const HeaderPhotoDesc = () => {
  return template({image: logo});
};

export default HeaderPhotoDesc;
