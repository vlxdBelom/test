import template from './header-menu.hbs';
import './header-menu.scss';
import background from '../../../assets/images/background.jpg';
import logo from '../../../assets/images/logo.png';


const HeaderMenu = () => {
  return template({ backgroundImage: background, profileImage: logo });
};

export default HeaderMenu;
