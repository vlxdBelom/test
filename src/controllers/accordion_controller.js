import {Controller} from 'stimulus';

export default class extends Controller {
  toggle(event) {
    const header = event.currentTarget;
    const item = header.parentElement;
    item.classList.toggle('active');
  }
}
