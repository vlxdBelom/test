// controllers/radio_controller.js
import {Controller} from 'stimulus';

export default class extends Controller {
  static targets = ['radio'];

  toggle(event) {
    const radio = event.target;
    const isChecked = radio.checked;

    console.log(`Checkbox with ID ${radio} is now ${isChecked ? 'checked' : 'unchecked'}`);
  }


}
