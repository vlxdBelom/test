import { Controller } from "stimulus";

export default class extends Controller {
  static targets = ["input", "label", "arrow", "list", "errorText"];

  connect() {
    this.updateState();
  }

  toggleDropdown() {
    this.listTarget.style.display = this.listTarget.style.display === 'block' ? 'none' : 'block';
    this.element.classList.toggle("show-list");
    this.updateLabelState();
  }

  selectOption(event) {
    const selectedOption = event.target.dataset.value;
    this.inputTarget.value = selectedOption;
    this.listTarget.style.display = 'none';
    this.element.classList.remove("show-list");
    this.element.classList.add("filled");
    this.updateLabelState();
  }

  updateState() {
    const input = this.inputTarget;
    if (input.value) {
      this.element.classList.add("filled");
    } else {
      this.element.classList.remove("filled");
    }
  }

  updateLabelState() {
    const input = this.inputTarget;
    const label = this.labelTarget;
    if (input.value || this.element.classList.contains("show-list")) {
      label.classList.add("float");
    } else {
      label.classList.remove("float");
    }
  }
}
