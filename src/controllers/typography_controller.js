import {Controller} from "stimulus";

export default class extends Controller {
  static targets = ["desktop", "mobile", "tab"];

  connect() {
    this.showDesktop();
  }

  desktop() {
    this.showDesktop();
  }

  phone() {
    this.showPhone();
  }

  showDesktop() {
    this.tabTargets.forEach(tab => {
      tab.classList.remove("active");
    });
    this.element.querySelector('button[data-action="click->typography#desktop"]').classList.add("active");
    this.element.querySelector('button[data-action="click->typography#phone"]').classList.remove("active");
    this.desktopTarget.classList.add("active");
    this.mobileTarget.classList.remove("active");
  }

  showPhone() {
    this.tabTargets.forEach(tab => {
      tab.classList.remove("active");
    });
    this.element.querySelector('button[data-action="click->typography#phone"]').classList.add("active");
    this.element.querySelector('button[data-action="click->typography#desktop"]').classList.remove("active");
    this.mobileTarget.classList.add("active");
    this.desktopTarget.classList.remove("active");
  }
}
