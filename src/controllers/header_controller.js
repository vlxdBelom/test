import {Controller} from "stimulus";

export default class extends Controller {
  showMenu() {
    this.element.getElementsByClassName('menu')[0].style.display = 'grid';
  }

  closeMenu() {
    this.element.getElementsByClassName('menu')[0].style.display = 'none';
  }
}
