// text_field_controller.js
import { Controller } from "stimulus";

export default class extends Controller {
  static targets = ["input", "label", "errorText"];

  connect() {
    this.updateState();
    this.validateEmail();
  }

  onFocus() {
    this.element.classList.add("focus");
    this.updateLabelState();
  }

  onBlur() {
    this.element.classList.remove("focus");
    this.updateState();
    this.validateEmail();
    this.updateLabelState();
  }

  onInput(event) {
    this.updateState();
    this.validateEmail();
    this.updateLabelState();
  }

  updateState() {
    const input = this.inputTarget;
    if (input.value) {
      this.element.classList.add("filled");
    } else {
      this.element.classList.remove("filled");
    }
  }

  updateLabelState() {
    const input = this.inputTarget;
    const label = this.labelTarget;
    if (input.value || this.element.classList.contains("focus")) {
      label.classList.add("float");
    } else {
      label.classList.remove("float");
    }
  }

  validateEmail() {
    const input = this.inputTarget;
    const errorText = this.errorTextTarget;
    const emailPattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;

    if (input.value && !emailPattern.test(input.value)) {
      this.element.classList.add("error");
      errorText.style.display = 'block';
    } else {
      this.element.classList.remove("error");
      errorText.style.display = 'none';
    }
  }
}
