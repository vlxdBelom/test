// controllers/checkbox_controller.js
import {Controller} from 'stimulus';

export default class extends Controller {
  static targets = ['checkbox'];

  toggle(event) {
    const checkbox = event.target;
    const isChecked = checkbox.checked;
  }
}
