import '../sass/globals.scss';
import {Application} from "@hotwired/stimulus";
import {definitionsFromContext} from "@hotwired/stimulus-webpack-helpers";

import Header from "../src/components/header/header";
import Layout from "../src/layout";
import TaskDescription from "../src/components/content/task-description/task-description";
import LeftMenu from "../src/components/left-menu/left-menu";
import Grid from "../src/components/content/grid/grid";
import Typography from "../src/components/content/typography/typography";
import Ui from "../src/components/content/ui/ui";


document.addEventListener('DOMContentLoaded', () => {
  const app = document.getElementById('app');
  window.Stimulus = Application.start();
  const context = require.context("../src/controllers", true, /\.js$/);
  Stimulus.load(definitionsFromContext(context));

  const header = Header();
  const taskDescription = TaskDescription();
  const leftMenu = LeftMenu();
  const grid = Grid();
  const typography = Typography();
  const ui = Ui();

  const layoutContent = Layout({header, children: [taskDescription, leftMenu, grid, typography, ui].join('')});

  app.innerHTML = layoutContent;
});
